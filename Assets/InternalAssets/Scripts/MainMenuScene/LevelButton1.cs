﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelButton1 : LevelLoader
{
    [SerializeField] private GameObject tickLevel;

    private void Start()
    {
        if (PlayerPrefs.GetInt("LevelCompletedLevel1") == 1)
        {
            tickLevel.SetActive(true);
        }
        else
        {
            tickLevel.SetActive(false);
        }
    }

    public override void LoadLevel()
    {
        PlayerPrefs.SetInt("IsGameSaved", 0);
        PlayerPrefs.SetString("currentLevelLoaded", "Level1");
        SceneManager.LoadScene("GamePlay");
    }
}