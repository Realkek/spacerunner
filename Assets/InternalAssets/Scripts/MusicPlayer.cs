﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    private static MusicPlayer _instance;

    private void Awake()
    { 
        if (_instance != null)
            Destroy(gameObject);
        else
        {
            _instance = this;
        } 
    }

    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
    
}