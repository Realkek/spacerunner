﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script moves the attached object along the Y-axis with the defined speed
/// </summary>
public class DirectMoving : MonoBehaviour
{
    [Tooltip("Moving speed on Y axis in local space")]
    public float speed;

    private float _heightCam;
    private Vector2 _centrCam; // центр камеры (это ее пивот)

    private static float _maxY; // левая и правая границы

    private void Start()
    {
        if (Camera.main != null)
        {
            var main = Camera.main;
            _heightCam = main.orthographicSize;

            _centrCam = main.transform
                .position; // получаем центр камеры, т.к. пивот у камеры по умолчанию в центре, центром будет ее позиция
        }

        _maxY = _centrCam.y + _heightCam;
    }

    //moving the object with the defined speed
    private void Update()
    {
        if (transform.position.y < _maxY)
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        else
        {
            Destroy(gameObject);
        }
    }
}