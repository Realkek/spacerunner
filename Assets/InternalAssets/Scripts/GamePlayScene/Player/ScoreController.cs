﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreController : MonoBehaviour, IEventSub, IEventTrigger
{
    [SerializeField] private TextMeshProUGUI scoreText;
    private int _score;

    // Start is called before the first frame update
    void Start()
    {
        _score = Convert.ToInt32(scoreText.text);
        Subscribe();
    }
    

    void DecrementScore()
    {
        _score--;
        scoreText.text = _score.ToString();
    }

    void CheckScore()
    {
        if (_score < 2)
        {
            TriggerEvent("LevelCompleted");
            scoreText.gameObject.SetActive(false);
            gameObject.SetActive(false);
        }
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("HazardDestroyed", CheckScore);
        ManagerEvents.StartListening("HazardDestroyed", DecrementScore);
        ManagerEvents.StartListening("GameOver", HideScore);
    }

    private void HideScore()
    {
        scoreText.gameObject.SetActive(false);
    }

    public void UnSubscribe()
    {
        throw new System.NotImplementedException();
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}