﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpaceShipShooting : MonoBehaviour, ILevelDataReceiver
{
    [SerializeField] private GameObject spaceshipGun;
    [SerializeField] private ParticleSystem spaceshipGunVfx;
    [SerializeField] private GameObject projectileObject;
    [SerializeField] private AudioClip playerShootingAudioClip;
    [SerializeField] private List<LevelData> levelData;
    private AudioSource _playerShootingAudioSource;

    private float _nextFireTime;
    private float _fireRateModifier = 0.7f;
    bool isActiveShooting = true;
    public static SpaceShipShooting instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        _playerShootingAudioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        ReceiveLevelData();
        spaceshipGunVfx = spaceshipGun.GetComponent<ParticleSystem>();
    }

    public void ReceiveLevelData()
    {
        string currentLevel = PlayerPrefs.GetString("currentLevelLoaded");
        switch (currentLevel)
        {
            case "Level1":
                _fireRateModifier = levelData[0].FireRateModifier;
                break;
            case "Level2":
                _fireRateModifier = levelData[1].FireRateModifier;
                break;
            case "Level3":
                _fireRateModifier = levelData[2].FireRateModifier;
                break;
        }
    }

    private void Update()
    {
        if (isActiveShooting)
        {
            if (Time.time > _nextFireTime)
            {
                MakeAShot();
                if (Math.Abs(_fireRateModifier) > 0)
                    _nextFireTime = Time.time + _fireRateModifier;
            }
        }
    }

    void MakeAShot()
    {
        _playerShootingAudioSource.PlayOneShot(playerShootingAudioClip);
        InstantiateRayShotPrefab(projectileObject, spaceshipGun.transform.position, Vector3.zero);
        spaceshipGunVfx.Play();
    }

    void InstantiateRayShotPrefab(GameObject shotRayPrefab, Vector3 instantiatePosition,
        Vector3 rotationCount)
    {
        Instantiate(shotRayPrefab, instantiatePosition, Quaternion.Euler(rotationCount));
    }
}