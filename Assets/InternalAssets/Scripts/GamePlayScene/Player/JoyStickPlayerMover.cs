﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Borders
{
    [Tooltip("offset from viewport borders for player's movement")]
    public float minXOffset = 1.5f, maxXOffset = 1.5f, minYOffset = 1.5f, maxYOffset = 1.5f;

    [HideInInspector] public float minX, maxX, minY, maxY;
}

public class JoyStickPlayerMover : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    public float speed;
    public VariableJoystick variableJoystick;
    public Rigidbody2D rb;

    [Tooltip("offset from viewport borders for player's movement")]
    public Borders borders;


    private void Start()
    {
        ResizeBorders();
    }


    private void Update()
    {
        MoveSpaceShip();
    }

    void MoveSpaceShip()
    {
        Vector3 direction = Vector3.up * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
        transform.position = transform.position + (direction * speed * Time.fixedDeltaTime);
        transform.position = Vector2.Lerp(transform.position,
            transform.position + (direction * speed * Time.fixedDeltaTime), 1.0f);
        transform.position = new Vector3 //if 'Player' crossed the movement borders, returning him back 
        (
            Mathf.Clamp(transform.position.x, borders.minX, borders.maxX),
            Mathf.Clamp(transform.position.y, borders.minY - 1, borders.maxY),
            0
        );
    }

    void ResizeBorders()
    {
        borders.minX = mainCamera.ViewportToWorldPoint(Vector2.zero).x + borders.minXOffset;
        borders.minY = mainCamera.ViewportToWorldPoint(Vector2.zero).y + borders.minYOffset;
        borders.maxX = mainCamera.ViewportToWorldPoint(Vector2.right).x - borders.maxXOffset;
        borders.maxY = mainCamera.ViewportToWorldPoint(Vector2.up).y - borders.maxYOffset;
    }
}