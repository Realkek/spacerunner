﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script defines which sprite the 'Player" uses and its health.
/// </summary>
public class Player : MonoBehaviour, IEventSub, IEventTrigger
{
    public GameObject destructionFX;
    [SerializeField] private GameObject damageFX;
    [SerializeField] private GameObject indicators;
    [SerializeField] private AudioClip _playerExplosionAudioClip;
    [SerializeField] private AudioSource playerAudioSource;

    private int damageNumber;

    public static Player instance;

    private void Awake()
    {
        Subscribe();
        if (instance == null)
            instance = this;
    }

    public void GetDamage()
    {
        switch (damageNumber)
        {
            case 0:
                indicators.transform.GetChild(damageNumber).gameObject.SetActive(false);
                Damage();
                break;
            case 1:
                indicators.transform.GetChild(damageNumber).gameObject.SetActive(false);
                Damage();
                break;
            case 2:
                indicators.transform.GetChild(damageNumber).gameObject.SetActive(false);
                Destruction();
                TriggerEvent("GameOver");
                break;
        }

        damageNumber++;
    }

    //'Player's' destruction procedure
    void Destruction()
    {
        playerAudioSource.PlayOneShot(_playerExplosionAudioClip);
        indicators.transform.GetChild(2).gameObject.SetActive(false);
        Instantiate(destructionFX, transform.position,
            Quaternion.identity); //generating destruction visual effect and destroying the 'Player' object
        Destroy(gameObject);
    }

    void Damage()
    {
        Instantiate(damageFX, transform.position,
            Quaternion.identity); //generating destruction visual effect and destroying the 'Player' object
    }

    void DisableSpaceshipResponse()
    {
        GetComponent<SpaceShipShooting>().enabled = false;
        GetComponent<Player>().enabled = false;
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("PlayerHasBeenDamaged", GetDamage);
        ManagerEvents.StartListening("LevelCompleted", DisableSpaceshipResponse);
    }

    public void UnSubscribe()
    {
        throw new System.NotImplementedException();
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}