﻿using System.Collections;
using UnityEngine;

public class Rigid2DMover : MonoBehaviour
{
    private Rigidbody2D _rigidbody2D;
    private int _horizontalSpeed = 0;
    private float _verticalSpeed = -4;
    private int _isHorizontalMovement;

    void Start()
    {
        _isHorizontalMovement = Random.Range(0, 4);
        if (_isHorizontalMovement == 1)
            _horizontalSpeed = Random.Range(-3, 3);
        _rigidbody2D = GetComponent<Rigidbody2D>();
        StartCoroutine(VerticalSpeedAcceleration());
    }

    void MoveHazard()
    {
        _rigidbody2D.velocity = new Vector3(_horizontalSpeed, _verticalSpeed, 0);
    }

    private IEnumerator VerticalSpeedAcceleration()
    {
        while (true)
        {
            yield return new WaitForSeconds(5);
            _verticalSpeed = _verticalSpeed * 1.2f;
        }
    }


    private void FixedUpdate()
    {
        MoveHazard();
    }
}