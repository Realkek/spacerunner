﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetsSpawner : MonoBehaviour
{
    public GameObject[] planets;
    public float timeBetweenPlanets;
    public float planetsSpeed;
    List<GameObject> planetsList = new List<GameObject>();

    private void Start()
    {
        StartCoroutine(PlanetsCreation());
    }

    IEnumerator PlanetsCreation()
    {
        //Create a new list copying the array
        for (int i = 0; i < planets.Length; i++)
        {
            planetsList.Add(planets[i]);
        }

        yield return new WaitForSeconds(8);
        while (true)
        {
            ////choose random object from the list, generate and delete it
            int randomIndex = Random.Range(0, planetsList.Count);
            GameObject newPlanet = Instantiate(planetsList[randomIndex]);
            planetsList.RemoveAt(randomIndex);
            //if the list decreased to zero, reinstall it
            if (planetsList.Count == 0)
            {
                for (int i = 0; i < planets.Length; i++)
                {
                    planetsList.Add(planets[i]);
                }
            }

            newPlanet.GetComponent<PlanetMoving>().speed = planetsSpeed;
            yield return new WaitForSeconds(timeBetweenPlanets);
        }
    }
}