﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour, IEventSub, IEventTrigger, IScreenDataReceiver, ILevelDataReceiver
{
    public AudioSource audioSource;
    public AudioClip winSound;
    public AudioClip looseSound;
    public GameObject[] hazards;
    public Vector3 spawnValues;

    [SerializeField] private GameObject scorePanel;
    [SerializeField] private GameObject backgroundThemeTop;
    [SerializeField] private GameObject backgroundThemeBottom;
    [SerializeField] private List<LevelData> levelData;
    [SerializeField] private GameObject healthIndicators;

    protected static float MinX, MaxX; // левая и правая границы

    private int _healthCount;
    private int _nextLevelNumber;
    private float _enemyWaitSpawnTime;
    private string _currentLevel;
    private float _widthCam; // ширина камеры
    private Vector2 _centrCam; // центр камеры (это ее пивот)


    private void Awake()
    {
        InitializationLevelData();
        CheckSaveState();
    }

    void Start()
    {
        Subscribe();
        GetScreenValues();
        StartCoroutine(WaitAndSpawn(_enemyWaitSpawnTime));
    }

    public void GetScreenValues()
    {
        if (Camera.main != null)
        {
            var main = Camera.main;
            _widthCam = main.orthographicSize *
                        main.aspect; // Получаем половину ширины камеры, путем умножения высоты на соотношение
            _centrCam = main.transform
                .position; // получаем центр камеры, т.к. пивот у камеры по умолчанию в центре, центром будет ее позиция
        }

        MinX = _centrCam.x - _widthCam; // левый край  (отнимаем от центра половину ширины)
        MaxX = _centrCam.x + _widthCam; // правый край (прибавляем к центру половину ширины)
    }

    public void ReceiveLevelData()
    {
        _currentLevel = PlayerPrefs.GetString("currentLevelLoaded");
        switch (_currentLevel)
        {
            case "Level1":
                scorePanel.GetComponent<TextMeshProUGUI>().text = levelData[0].HowMuchScorePoints.ToString();
                _nextLevelNumber = levelData[0].LevelNumber + 1;
                _enemyWaitSpawnTime = levelData[0].EnemyWaitSpawnTime;
                backgroundThemeTop.GetComponent<SpriteRenderer>().sprite = levelData[0].BackgroundTop;
                backgroundThemeBottom.GetComponent<SpriteRenderer>().sprite = levelData[0].BackgroundBottom;
                break;
            case "Level2":
                scorePanel.GetComponent<TextMeshProUGUI>().text = levelData[1].HowMuchScorePoints.ToString();
                _nextLevelNumber = levelData[0].LevelNumber + 2;
                _enemyWaitSpawnTime = levelData[1].EnemyWaitSpawnTime;
                backgroundThemeTop.GetComponent<SpriteRenderer>().sprite = levelData[1].BackgroundTop;
                backgroundThemeBottom.GetComponent<SpriteRenderer>().sprite = levelData[1].BackgroundBottom;
                break;
            case "Level3":
                scorePanel.GetComponent<TextMeshProUGUI>().text = levelData[2].HowMuchScorePoints.ToString();
                _nextLevelNumber = levelData[0].LevelNumber + 1;
                _enemyWaitSpawnTime = levelData[2].EnemyWaitSpawnTime;
                backgroundThemeTop.GetComponent<SpriteRenderer>().sprite = levelData[2].BackgroundTop;
                backgroundThemeBottom.GetComponent<SpriteRenderer>().sprite = levelData[2].BackgroundBottom;
                break;
        }

        TriggerEvent("LevelInitialized");
    }

    private void InitializationLevelData()
    {
        ReceiveLevelData();
    }

    void Update()
    {
        CheckQuitToMenu();
    }

    private void SetHealthCount()
    {
        switch (_healthCount)
        {
            case 1:
                healthIndicators.transform.GetChild(0).gameObject.SetActive(true);
                break;
            case 2:
                healthIndicators.transform.GetChild(0).gameObject.SetActive(true);
                healthIndicators.transform.GetChild(1).gameObject.SetActive(true);
                break;
            case 3:
                healthIndicators.transform.GetChild(0).gameObject.SetActive(true);
                healthIndicators.transform.GetChild(1).gameObject.SetActive(true);
                healthIndicators.transform.GetChild(2).gameObject.SetActive(true);
                break;
        }
    }

    private void CheckSaveState()
    {
        if (PlayerPrefs.GetInt("IsGameSaved") == 1)
        {
            scorePanel.GetComponent<TextMeshProUGUI>().text =
                PlayerPrefs.GetString("SavedScore");
            _healthCount = PlayerPrefs.GetInt("SavedHealth");
            SetHealthCount();
            PlayerPrefs.SetInt("IsGameSaved", 0);
        }
        else
        {
            _healthCount = 3;
            SetHealthCount();
        }
    }

    private void CheckQuitToMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PlayerPrefs.SetString("SavedScore", scorePanel.GetComponent<TextMeshProUGUI>().text);
            CheckHealthCount();
            PlayerPrefs.SetInt("SavedHealth", _healthCount);
            PlayerPrefs.SetInt("IsGameSaved", 1);
            SceneManager.LoadScene("MainMenu");
        }
    }

    void CheckHealthCount()
    {
        _healthCount = 0;
        for (int healthCounter = 0; healthCounter < healthIndicators.transform.childCount; healthCounter++)
        {
            if (healthIndicators.transform.GetChild(healthCounter).gameObject.activeSelf)
                _healthCount++;
        }
    }

    private IEnumerator WaitAndSpawn(float waitTime)
    {
        yield return new WaitForSeconds(0.2f);
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            SpawnHazard();
        }
    }

    void SpawnHazard()
    {
        GameObject hazard = hazards[UnityEngine.Random.Range(0, hazards.Length)];
        Vector3 spawnPosition = new Vector3(UnityEngine.Random.Range(MinX + 0.5f, MaxX - 0.5f), spawnValues.y,
            spawnValues.z);
        Quaternion spawnRotation = Quaternion.identity;
        Instantiate(hazard, spawnPosition, spawnRotation);
    }


    private void DisableLevelController()
    {
        PlayerPrefs.SetInt("IsGameSaved", 0);
        gameObject.SetActive(false);
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("GameOver", DisableLevelController);
        ManagerEvents.StartListening("GameOver", PlayLooseSound);
        ManagerEvents.StartListening("LevelCompleted", OpenNewLevel);
        ManagerEvents.StartListening("LevelCompleted", PlayWinSound);
        ManagerEvents.StartListening("LevelCompleted", DisableLevelController);
    }

    private void PlayLooseSound()
    {
        audioSource.PlayOneShot(looseSound);
    }

    private void PlayWinSound()
    {
        audioSource.PlayOneShot(winSound);
    }

    private void OpenNewLevel()
    {
        PlayerPrefs.SetInt($"LevelCompleted{_currentLevel}", 1);
        PlayerPrefs.SetInt($"LevelOpened{_nextLevelNumber}", 1);
    }

    public void UnSubscribe()
    {
        throw new NotImplementedException();
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}