﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsCounter : MonoBehaviour, IEventSub, IEventTrigger
{
    private void Start()
    {
        Subscribe();
    }

    private void GetCoin()
    {
        int coinsCount = PlayerPrefs.GetInt("CoinsCount");
        coinsCount++;
        PlayerPrefs.SetInt("CoinsCount", coinsCount);
        TriggerEvent("CoinsCountChanged");
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("CoinReceived", GetCoin);
    }

    public void UnSubscribe()
    {
        ManagerEvents.StopListening("CoinReceived", GetCoin);
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}