﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinsText : MonoBehaviour, IEventSub
{
    private TextMeshProUGUI _textMeshProUgui;

    private void Awake()
    {
        _textMeshProUgui = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        ShowCoinsCount();
        Subscribe();
    }

    private void ShowCoinsCount()
    {
        string coinsText = PlayerPrefs.GetInt("CoinsCount").ToString();
        _textMeshProUgui.text = coinsText;
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("CoinsCountChanged", ShowCoinsCount);
    }

    public void UnSubscribe()
    {
        ManagerEvents.StopListening("CoinsCountChanged", ShowCoinsCount);
    }
}