﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsRotator : MonoBehaviour
{
    void Start()
    {
        GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-300f, 300f);
    }
}