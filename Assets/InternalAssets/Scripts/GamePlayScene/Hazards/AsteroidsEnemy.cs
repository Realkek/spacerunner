﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsEnemy : MonoBehaviour
{
    [SerializeField] private int health;

    [SerializeField] private GameObject destructionVfx;

    public void GetDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
            Destruction();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TriggerEvent("PlayerHasBeenDamaged");
            Destruction();
        }
    }

    void Destruction()
    {
        Instantiate(destructionVfx, transform.position, Quaternion.identity);
        TriggerEvent("HazardDestroyed");
        Destroy(gameObject);
    }

    private void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}