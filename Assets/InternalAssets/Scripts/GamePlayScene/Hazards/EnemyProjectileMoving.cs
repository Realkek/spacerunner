﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileMoving : MonoBehaviour, IScreenDataReceiver, ITick
{
    private float _speed = -8;
    private float _heightCam;
    private Vector2 _centrCam; // центр камеры (это ее пивот)

    private static float _minY; // левая и правая границы

    private void Start()
    {
        ManagerUpdate.AddTo(this);
        GetScreenValues();
    }


    private void MoveProjectile()
    {
        if (transform.position.y > _minY)
            transform.Translate(Vector3.up * _speed * Time.deltaTime);
        else
        {
            ManagerUpdate.RemoveFrom(this);
            Destroy(gameObject);
        }
    }

    public void GetScreenValues()
    {
        if (Camera.main != null)
        {
            var main = Camera.main;
            _heightCam = main.orthographicSize;

            _centrCam = main.transform
                .position; // получаем центр камеры, т.к. пивот у камеры по умолчанию в центре, центром будет ее позиция
        }

        _minY = _centrCam.y - _heightCam;
    }

    public void Tick()
    {
        if (this != null)
            MoveProjectile();
        else
        {
            ManagerUpdate.RemoveFrom(this);
        }
    }
}