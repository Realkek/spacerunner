﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Enemy : MonoBehaviour, IEventTrigger, IDamageable, IDestructible, IShooter, ILevelDataReceiver
{
    [SerializeField] private int health;
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] GameObject destructionEnemyVfx;
    [SerializeField] private List<LevelData> levelData;
    [SerializeField] private GameObject hitEffect;
    private float _shooterActivateWaitingTime;

    private void Start()
    {
        ReceiveLevelData();
        StartCoroutine(ActivateShooter(_shooterActivateWaitingTime));
    }

    public void ReceiveLevelData()
    {
        string currentLevel = PlayerPrefs.GetString("currentLevelLoaded");
        switch (currentLevel)
        {
            case "Level1":
                _shooterActivateWaitingTime = levelData[0].ShooterActivateWaitingTime;
                break;
            case "Level2":
                _shooterActivateWaitingTime = levelData[1].ShooterActivateWaitingTime;
                break;
            case "Level3":
                _shooterActivateWaitingTime = levelData[2].ShooterActivateWaitingTime;
                break;
        }
    }

    private IEnumerator ActivateShooter(float waitingTime)
    {
        yield return new WaitForSeconds(waitingTime);
        if (this != null)
            MakeAShot();
    }

    public void MakeAShot()
    {
        Instantiate(projectilePrefab, gameObject.transform.position, Quaternion.identity);
    }

    public void GetDamage(int damage)
    {
        health -= damage; //reducing health for damage value, if health is less than 0, starting destruction procedure
        if (health <= 0)
            Destruction();
        else
            Instantiate(hitEffect, transform.position, Quaternion.identity, transform);
    }

    //if 'Enemy' collides 'Player', 'Player' gets the damage equal to projectile's damage value
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TriggerEvent("PlayerHasBeenDamaged");
            Destruction();
        }
    }

    public void Destruction()
    {
        Instantiate(destructionEnemyVfx, transform.position, Quaternion.identity);
        TriggerEvent("HazardDestroyed");
        Destroy(gameObject);
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}