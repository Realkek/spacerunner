﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "DataStore/LevelData")]
public class LevelData : ScriptableObject
{
    [SerializeField] private int levelNumber;
    [SerializeField] private int howMuchScorePoints;
    [SerializeField] private Sprite backgroundTop;
    [SerializeField] private Sprite backgroundBottom;
    [SerializeField] private float shooterActivateWaitingTime;
    [SerializeField] private float enemyWaitSpawnTime;
    [SerializeField] private float fireRateModifier;
    public int LevelNumber => levelNumber;
    public float ShooterActivateWaitingTime => shooterActivateWaitingTime;
    public float FireRateModifier => fireRateModifier;
    public float EnemyWaitSpawnTime => enemyWaitSpawnTime;
    public int HowMuchScorePoints => howMuchScorePoints;
    public Sprite BackgroundTop => backgroundTop;
    public Sprite BackgroundBottom => backgroundBottom;
}